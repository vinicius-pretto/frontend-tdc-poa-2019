const theme = {
  colors: {
    gray: {
      600: '#ccc',
      700: '#333',
      800: '#222',
      900: '#121212'
    },
    white: '#FFF',
    orange: {
      900: '#e1882e'
    },
    red: {
      900: '#D02A2A'
    },
    blue: {
      900: '#1334DE'
    }
  },
  fonts: {
    primary: '"Open Sans", Arial, sans-serif'
  }
}

export default theme;
