import axios from 'axios';

const submitTalk = talk => {
  return axios.post('http://localhost:8080/talk', talk);
}

export default {
  submitTalk
}
