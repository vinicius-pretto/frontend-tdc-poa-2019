import React from "react";
import styled from "styled-components";

import ErrorMessage from '../ErrorMessage';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 16px;
`;

const Label = styled.label`
  font-size: 15px;
  font-weight: bold;
  margin-bottom: 5px;
  color: ${props => props.theme.colors.gray[700]};
`;

const TextAreaStyled = styled.textarea`
  height: auto;
  line-height: 1.428571429;
  padding: 10px 18px;
  font-size: 15px;
  font-family: ${props => props.theme.fonts.primary};
  border: 1px solid ${props => props.theme.colors.gray[600]};
  box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
  color: ${props => props.theme.colors.gray[700]};
  width: 100%;
  transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;

  &:placeholder {
    color: ${props => props.theme.colors.gray[600]};
  }
`;

const TextArea = (props) => {
  const { 
    id, 
    cols, 
    rows,
    label, 
    required, 
    placeholder, 
    value,
    error,
    touched,
    onChange,
    onBlur
  } = props;
  const hasError = error && touched;

  return (
    <Container>
      <Label htmlFor={id}>
        {label}
      </Label>
      <TextAreaStyled
        id={id}
        cols={cols}
        rows={rows}
        data-cy={id}
        required={required}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        onBlur={onBlur}
      />
      {hasError && <ErrorMessage>{error}</ErrorMessage>}
    </Container>
  );
};

export default TextArea;
