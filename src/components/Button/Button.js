import styled from 'styled-components';
import Device from '../../styles/Device';

const Button = styled.button`
  min-width: 140px;
  background-color: ${props => props.theme.colors.orange[900]};
  border: 1px solid ${props => props.theme.colors.orange[900]};
  border-radius: 4px;
  padding: 15px 25px;
  color: ${props => props.theme.colors.white};
  font-size: 15px;
  font-weight: bold;
  cursor: pointer;

  @media ${Device.MOBILE} {
    width: 100%;
    font-size: 1rem;
  }
`

export default Button;
