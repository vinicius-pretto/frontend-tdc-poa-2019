import styled from 'styled-components';

const Label = styled.label`
  font-size: 15px;
  font-weight: bold;
  margin-bottom: 5px;
  color: ${props => props.theme.colors.gray[700]};
`;

export default Label;
