import React from 'react';
import styled from 'styled-components';

import Container from '../Container';

const HeaderStyled = styled.header`
  padding-top: 14.5px;
  padding-bottom: 14.5px;
  background: ${props => props.theme.colors.gray[800]};
  border-color: ${props => props.theme.colors.gray[900]};
  color: ${props => props.theme.colors.white};
  font-family: ${props => props.theme.fonts.primary};
  font-size: 19px;
  font-weight: bold;
  line-height: 21px;
`

const Year = styled.span`
  color: ${props => props.theme.colors.orange[900]};
`

const Header = () => {
  return (
    <HeaderStyled>
      <Container>
        TDC<Year>2019</Year> POA CFP
      </Container>
    </HeaderStyled>
  )
}

export default Header;
