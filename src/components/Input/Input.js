import React from "react";
import styled from "styled-components";

import ErrorMessage from '../ErrorMessage';
import Label from '../Label';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 16px;
`;

const InputStyled = styled.input`
  height: 43px;
  line-height: 1.428571429;
  padding: 10px 18px;
  font-size: 15px;
  font-family: ${props => props.theme.fonts.primary};
  border: 1px solid ${props => props.theme.colors.gray[600]};
  box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
  color: ${props => props.theme.colors.gray[700]};
  width: 100%;

  &:placeholder {
    color: ${props => props.theme.colors.gray[600]};
  }
`;

const Input = (props) => {
  const { 
    id, 
    type, 
    label, 
    required, 
    placeholder, 
    value,
    error,
    touched,
    onChange,
    onBlur
  } = props;
  const hasError = error && touched;

  return (
    <Container>
      <Label htmlFor={id}>
        {label}
      </Label>
      <InputStyled
        id={id}
        data-cy={id}
        type={type}
        required={required}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        onBlur={onBlur}
      />
      {hasError && <ErrorMessage>{error}</ErrorMessage>}
    </Container>
  );
};

export default Input;
