import React from 'react';
import styled from "styled-components";

import ErrorMessage from '../ErrorMessage';
import Label from '../Label';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  margin-bottom: 16px;
`

const SelectStyled = styled.select`
  -webkit-appearance: none;
  -moz-appearance: none;
  -ms-appearance: none;
  appearance: none;
  display: flex;
  flex: 1;
  border: none;
  background: transparent;
  outline: none;
  height: 43px;
  line-height: 1.428571429;
  padding: 10px 18px;
  font-size: 15px;
  color: ${props => props.theme.colors.gray[700]};
  font-family: ${props => props.theme.fonts.primary};
`

const SelectContainer = styled.div`
  display: flex;
  position: relative;
  overflow: hidden;
  max-height: 48px;
  background: #FFF;
  border-width: 1px;
  border-style: solid;
  border-color: ${props => props.theme.colors.gray[600]};
  box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
`

const ArrowIcon = styled.span`
  content: '';
  position: absolute;
  right: 10px;
  top: 33%;
  width: 10px;
  height: 10px;
  border-style: solid;
  border-width: 0 0 2px 2px;
  border-color: ${props => props.theme.colors.gray[700]};
  transform: rotate(-45deg);
  pointer-events: none;
`

const Select = (props) => {
  const { 
    id, 
    name, 
    value, 
    label, 
    options, 
    style, 
    required, 
    error,
    touched,
    onChange,
    onBlur
  } = props;
  const hasError = error && touched;

  return (
    <Container style={style}>
      <Label htmlFor={id}>
        {label}
      </Label>
      
      <SelectContainer>
        <SelectStyled
          id={id} 
          data-cy={id}
          name={name} 
          value={value} 
          onChange={onChange} 
          onBlur={onBlur}
          required={required}
        >
          {options.map((option, key) => 
            <option key={key} value={option.value}>{option.name}</option>
          )}
        </SelectStyled>
        <ArrowIcon />
      </SelectContainer>

      {hasError && <ErrorMessage>{error}</ErrorMessage>}
    </Container>
  )
};

export default Select;

