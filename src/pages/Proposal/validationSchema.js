import * as yup from 'yup';

const validationSchema = yup.object().shape({
  title: yup
    .string()
    .min(3, 'O campo deve ter entre 3 e 125 caracteres')
    .max(125, 'O campo deve ter entre 3 e 125 caracteres')
    .required('O campo não pode ser vazio'),
  summary: yup
    .string()
    .min(3, 'O campo deve ter entre 3 e 700 caracteres')
    .max(700, 'O campo deve ter entre 3 e 700 caracteres')
    .required('O campo não pode ser vazio'),
  messageForCommitte: yup
    .string()
    .min(3, 'O campo deve ter entre 3 e 3.500 caracteres')
    .max(3500, 'O campo deve ter entre 3 e 3.500 caracteres')
    .required('O campo não pode ser vazio'),
  language: yup
    .string()
    .required('O campo não pode ser vazio'),
  type: yup
    .string()
    .required('O campo não pode ser vazio'),
  track: yup
    .string()
    .required('O campo não pode ser vazio'),
});

export default validationSchema;
