import ProposalPage from '../support/ProposalPage';

describe('Submit a proposal', () => {
  let proposalPage;

  beforeEach(() => {
    proposalPage = new ProposalPage();
  });

  it('should submit a proposal', () => {
    proposalPage
      .visit()
      .fillForm()
      .submitProposal();
  });
});
