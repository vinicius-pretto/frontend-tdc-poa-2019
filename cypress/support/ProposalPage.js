import Routes from '../../src/consts/Routes';
import proposal from '../fixtures/proposal.json';

class ProposalPage {
  visit() {
    cy.visit(Routes.PROPOSAL);
    return this;
  }

  fillForm() {
    cy.get('[data-cy=title]').type(proposal.title);
    cy.get('[data-cy=summary]').type(proposal.summary);
    cy.get('[data-cy=messageForCommitte]').type(proposal.messageForCommitte);
    cy.get('[data-cy=language]').select(proposal.language);
    cy.get('[data-cy=type]').select(proposal.type);
    cy.get('[data-cy=track]').select(proposal.track);
    return this;
  }

  submitProposal() {
    cy.get('[data-cy=submit-button]').click();
    return this;
  }
}

export default ProposalPage;
